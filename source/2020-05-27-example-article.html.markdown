---
title: My First Article
date: 2020-05-27
tags: [example, first, initial]
---

This is my first article. This blog has been generated using [middleman](https://middlemanapp.com/basics/blogging/).

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

More to follow ....
